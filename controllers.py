"""
This file defines actions, i.e. functions the URLs are mapped into
The @action(path) decorator exposed the function at URL:

    http://127.0.0.1:8000/{app_name}/{path}

If app_name == '_default' then simply

    http://127.0.0.1:8000/{path}

If path == 'index' it can be omitted:

    http://127.0.0.1:8000/

The path follows the bottlepy syntax.

@action.uses('generic.html')  indicates that the action uses the generic.html template
@action.uses(session)         indicates that the action uses the session
@action.uses(db)              indicates that the action uses the db
@action.uses(T)               indicates that the action uses the i18n & pluralization
@action.uses(auth.user)       indicates that the action requires a logged in user
@action.uses(auth)            indicates that the action requires the auth object

session, db, T, auth, and tempates are examples of Fixtures.
Warning: Fixtures MUST be declared with @action.uses({fixtures}) else your app will result in undefined behavior
"""

from py4web import action, request, abort, redirect, URL
from yatl.helpers import A
from .common import db, session, T, cache, auth, logger, authenticated, unauthenticated, flash
from py4web.utils.url_signer import URLSigner
from .models import get_user_email

url_signer = URLSigner(session)

def get_user():
    return auth.current_user.get('id') if auth.current_user else None

def get_user_first_name():
    return auth.current_user.get('first_name') if auth.current_user else None

def get_user_last_name():
    return auth.current_user.get('last_name') if auth.current_user else None

@action('index')
@action.uses(db, auth.user, 'index.html')
def index():
    return dict(
        load_post_url = URL('load', signer=url_signer),
        add_post_url = URL('add_post', signer=url_signer),
        delete_post_url = URL('delete_post', signer=url_signer),
        get_rating_url = URL('get_rating', signer=url_signer),
        set_rating_url = URL('set_rating', signer=url_signer),
    )

@action('load')
@action.uses(db, auth.user, url_signer.verify())
def load():
    posts = db(db.post).select().as_list()
    return dict(posts = posts,
                user = get_user(),)

@action('add_post', method="POST")
@action.uses(db, auth.user, url_signer.verify())
def add_post():
    id = db.post.insert(
        content=request.json.get('content'),
    )
    return dict(id=id)

@action('delete_post')
@action.uses(db, auth.user, url_signer.verify())
def delete_post():
    id = request.params.get('id')
    assert id is not None
    db(db.post.id == id).delete()
    return "ok"

@action('get_rating')
@action.uses(db, auth.user, url_signer.verify())
def get_rating():
    post_id = int(request.params.get('post_id'))
    row = db((db.thumbs.post == post_id) & (db.thumbs.rater == get_user())).select().first()
    rating = row.rating if row is not None else 0
    return dict(rating=rating)

@action('set_rating', method="POST")
@action.uses(db, auth.user, url_signer.verify())
def set_rating():
    post_id = request.json.get('post_id')
    rating = request.json.get('rating')
    db.thumbs.update_or_insert(
        ((db.thumbs.post == post_id) & (db.thumbs.rater == get_user())),
        post=post_id,
        rating=rating,
        rater=get_user(),
    )
    return "ok"
