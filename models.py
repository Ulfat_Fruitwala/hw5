"""
This file defines the database models
"""

import datetime
from .common import db, Field, auth
from pydal.validators import *


def get_user_email():
    return auth.current_user.get('email') if auth.current_user else None

def get_user():
    return auth.current_user.get('id') if auth.current_user else None

def get_time():
    return datetime.datetime.utcnow()

db.define_table(
    'post',
    Field('content'),
    Field('author', default=get_user),
)

db.define_table(
    'thumbs',
    Field('post', 'reference post'),
    Field('rating', 'integer', default=0),
    Field('rater', 'reference auth_user', default=get_user)
)

db.commit()
