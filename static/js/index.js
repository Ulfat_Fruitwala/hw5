// This will be the object that will contain the Vue attributes
// and be used to initialize it.
let app = {};


// Given an empty app object, initializes it filling its attributes,
// creates a Vue instance, and then initializes the Vue instance.
let init = (app) => {

    // This is the Vue data.
    app.data = {
        posts: [],
        add_post: "",

        // User Info
        user: "",

        // Complete as you see fit.
    };

    app.add_a_post = function(){
        axios.post(add_post_url, {
            content: app.vue.add_post,
        }).then(function(response) {
            app.vue.posts.splice(0,0,{
                id: response.data.id,
                content: app.vue.add_post,
                author: app.vue.user,
                hover_rating: 0,
                true_rating: 0,
            });
            app.enumerate(app.vue.posts);
            app.vue.add_post = "";
        });
    };

    app.cancel_post = function(){
        app.vue.add_post = "";
    };

    app.enumerate = (a) => {
        // This adds an _idx field to each element of the array.
        let k = 0;
        a.map((e) => {e._idx = k++;});
        return a;
    };

    app.complete = (a) => {
        a.map((e) => {
            e.true_rating = 0;
            e.hover_rating = 0;
        });
        return a;
    };

    app.delete_post = function(id){
        axios.get(delete_post_url, {params: {id: id}}
        ).then(function (response){
            for(let i=0; i<app.vue.posts.length; i++){
                if(app.vue.posts[i].id === id){
                    app.vue.posts.splice(i, 1);
                    app.enumerate(app.vue.posts);
                    break;
                }
            }
        });
    };

    app.thumb_over = function (post_idx, thumb){
        let p = app.vue.posts[post_idx];
        // hover rating is 1
        if(p.hover_rating == 1){
            if(thumb == -1)     p.hover_rating = -1;
            else                p.hover_rating = 0;
        }
        // hover rating is -1
        else if(p.hover_rating == -1){
            if(thumb == 1)      p.hover_rating = 1;
            else                p.hover_rating = 0;
        }
        // hover rating is 0
        else{
            p.hover_rating = thumb;
        }
        app.vue.posts.splice(post_idx,1,p);
    };

    app.thumb_out = function(post_idx){
        let p = app.vue.posts[post_idx];
        p.hover_rating = p.true_rating;
        app.vue.posts.splice(post_idx,1,p);
    };

    app.set_thumb = function(post_idx, thumb){
        let p = app.vue.posts[post_idx];
        let rate = thumb;
        if(p.true_rating === thumb)  rate = 0;
        p.true_rating = rate;
        p.hover_rating = rate;
        axios.post(set_rating_url, {post_id: p.id, rating: rate});
        app.vue.posts.splice(post_idx,1,p);
    };

    // This contains all the methods.
    app.methods = {
        add_a_post: app.add_a_post,
        cancel_post: app.cancel_post,
        delete_post: app.delete_post,
        thumb_over: app.thumb_over,
        thumb_out: app.thumb_out,
        set_thumb: app.set_thumb,
    };

    // This creates the Vue instance.
    app.vue = new Vue({
        el: "#vue-target",
        data: app.data,
        methods: app.methods
    });

    // And this initializes it.
    app.init = () => {
        axios.get(load_post_url)
        .then(function (response){
            let p = response.data.posts;
            p = app.enumerate(p);
            p = app.complete(p);
            app.vue.posts = p;
            app.vue.user = response.data.user;
        })
        .then(() => {
            for (let p of app.vue.posts){
                axios.get(get_rating_url, {params: {post_id: p.id}})
                .then((result) => {
                    p.true_rating = result.data.rating;
                    p.hover_rating = p.true_rating;
                });
            }
        });
    };

    // Call to the initializer.
    app.init();
};

// This takes the (empty) app object, and initializes it,
// putting all the code i
init(app);
